﻿using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Interfaces.Repositories;
using BSBookingQuery.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Infrastructure.Repositories
{
    public class HotelRepository : BaseRepository<Hotel>, IHotelRepository
    {
        public HotelRepository(ApplicationDbContext context) : base(context) { }

        public async Task<IEnumerable<Hotel>> GetHotelsByName(string name)
        {
            return await _context.Set<Hotel>()
                .Where(h => h.Name.Contains(name))
                .ToListAsync();
        }

        public async Task<IEnumerable<Hotel>> GetHotelsByLocation(string location)
        {
            return await _context.Set<Hotel>()
                .Where(h => h.Location.Contains(location))
                .ToListAsync();
        }

        public async Task<IEnumerable<Hotel>> GetHotelsByRatingRange(int minRating, int maxRating)
        {
            return await _context.Set<Hotel>()
                .Where(h => h.Rating >= minRating && h.Rating <= maxRating)
                .ToListAsync();
        }
    }
}
