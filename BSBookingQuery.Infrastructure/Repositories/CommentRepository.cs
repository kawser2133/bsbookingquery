﻿using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Interfaces.Repositories;
using BSBookingQuery.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Infrastructure.Repositories
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context) { }

        public async Task<IEnumerable<Comment>> GetCommentsByHotelId(int hotelId)
        {
            return await _context.Set<Comment>()
                .Where(c => c.HotelId == hotelId)
                .ToListAsync();
        }
    }
}
