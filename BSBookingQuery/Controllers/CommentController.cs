﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Exceptions;
using BSBookingQuery.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BSBookingQuery.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ILogger<CommentController> _logger;
        private readonly ICommentService _commentService;

        public CommentController(ILogger<CommentController> logger, ICommentService commentService)
        {
            _logger = logger;
            _commentService = commentService;
        }

        [HttpGet("get-all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var comments = await _commentService.GetAllComments();
                return Ok(comments);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving all comment");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }

        }

        [HttpGet("get-by-hotelid/hotel/{hotelId}")]
        public async Task<IActionResult> GetByHotelId(int hotelId)
        {
            try
            {
                var comments = await _commentService.GetCommentsByHotelId(hotelId);
                return Ok(comments);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving comments with hotel id- {hotelId}");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpPost("add/hotel/{hotelId}")]
        public async Task<IActionResult> AddComment(int hotelId, [FromBody] CommentAddRequestViewModel addRequestViewModel)
        {
            try
            {
                var commentData = await _commentService.AddComment(hotelId, addRequestViewModel);
                return Ok(commentData);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while adding a comment");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpPost("reply/comment/{commentId}")]
        public async Task<IActionResult> ReplyComment(int commentId, [FromBody] ReplyAddRequestViewModel addRequestViewModel)
        {
            try
            {
                var commentData = await _commentService.AddReply(commentId, addRequestViewModel);
                return Ok(commentData);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while replying to a comment id- {commentId}");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] CommentUpdateRequestViewModel updateRequestViewModel)
        {
            if (id != updateRequestViewModel.Id)
            {
                return BadRequest();
            }

            try
            {
                await _commentService.UpdateComment(updateRequestViewModel);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while updating a comment with id- {updateRequestViewModel.Id}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _commentService.DeleteComment(id);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while deleting a comment with id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}