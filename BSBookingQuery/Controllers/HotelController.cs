﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Exceptions;
using BSBookingQuery.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BSBookingQuery.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        private readonly ILogger<HotelController> _logger;
        private readonly IHotelService _hotelService;

        public HotelController(ILogger<HotelController> logger, IHotelService hotelService)
        {
            _logger = logger;
            _hotelService = hotelService;
        }

        [HttpGet("get-by-id/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var hotel = await _hotelService.GetHotelById(id);
                return Ok(hotel);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving a hotel with id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpGet("get-all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var hotels = await _hotelService.GetAllHotels();
                return Ok(hotels);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving all hotel");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }

        }

        [HttpGet("get-by-name/name/{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            try
            {
                var hotels = await _hotelService.GetHotelsByName(name);
                return Ok(hotels);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving by name- {name}");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpGet("get-by-location/location/{location}")]
        public async Task<IActionResult> GetByLocation(string location)
        {
            try
            {
                var hotels = await _hotelService.GetHotelsByLocation(location);
                return Ok(hotels);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving by location- {location}");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpGet("get-by-rating/rating/{minRating}/{maxRating}")]
        public async Task<IActionResult> GetByRatingRange(int minRating, int maxRating)
        {
            try
            {
                var hotels = await _hotelService.GetHotelsByRatingRange(minRating, maxRating);
                return Ok(hotels);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while retrieving by rating, min- {minRating} and max- {maxRating}");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] HotelAddRequestViewModel addRequestViewModel)
        {
            try
            {
                var hotelData = await _hotelService.CreateHotel(addRequestViewModel);
                return Ok(hotelData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while adding a hotel");
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing your request.");
            }
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] HotelUpdateRequestViewModel updateRequestViewModel)
        {
            if (id != updateRequestViewModel.Id)
            {
                return BadRequest();
            }

            try
            {
                await _hotelService.UpdateHotel(updateRequestViewModel);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while updating a hotel with id- {updateRequestViewModel.Id}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _hotelService.DeleteHotel(id);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while deleting a hotel with id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}