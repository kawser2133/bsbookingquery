﻿using BSBookingQuery.Core.Interfaces.Mapper;
using BSBookingQuery.Core.Interfaces.Repositories;
using BSBookingQuery.Core.Interfaces.Services;
using BSBookingQuery.Core.Mapper;
using BSBookingQuery.Core.Services;
using BSBookingQuery.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSBookingQuery.Api.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            #region Repositories
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IHotelRepository, HotelRepository>();
            services.AddTransient<ICommentRepository, CommentRepository>();
            #endregion

            #region Services
            services.AddScoped<IHotelService, HotelService>();
            services.AddScoped<IHotelMapper, HotelMapper>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<ICommentMapper, CommentMapper>();
            #endregion

            return services;
        }
    }
}
