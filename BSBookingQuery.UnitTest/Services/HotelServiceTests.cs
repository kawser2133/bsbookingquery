﻿using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Interfaces.Mapper;
using BSBookingQuery.Core.Interfaces.Repositories;
using BSBookingQuery.Core.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSBookingQuery.UnitTest.Services
{
    public class HotelServiceTests
    {
        private Mock<IHotelRepository> _hotelRepositoryMock;
        private Mock<IHotelMapper> _mapperMock;

        [SetUp]
        public void Setup()
        {
            _hotelRepositoryMock = new Mock<IHotelRepository>();
            _mapperMock = new Mock<IHotelMapper>();
        }

        [Test]
        public async Task GetHotels_ReturnsListOfHotels()
        {
            // Arrange
            var hotels = new List<Hotel>
            {
                new Hotel { Id = 1, Name = "Hotel A", Location = "Location A", Rating = 4, Description="Hotel A Description", CreatedAt=DateTime.Now },
                new Hotel { Id = 2, Name = "Hotel B", Location = "Location B", Rating = 3, Description="Hotel B Description", CreatedAt=DateTime.Now.AddMinutes(1) }
            };

            _hotelRepositoryMock.Setup(repo => repo.GetAllAsync())
                                .ReturnsAsync(hotels);

            var hotelService = new HotelService(_hotelRepositoryMock.Object, _mapperMock.Object);

            // Act
            var result = await hotelService.GetAllHotels();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

    }
}
