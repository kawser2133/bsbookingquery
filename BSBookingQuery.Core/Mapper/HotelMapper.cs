﻿using AutoMapper;
using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Interfaces.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSBookingQuery.Core.Mapper
{
    public class HotelMapper : IHotelMapper
    {
        private readonly IMapper _mapper;

        public HotelMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Hotel, HotelViewModel>();
                cfg.CreateMap<HotelViewModel, Hotel>();
                cfg.CreateMap<Hotel, HotelAddRequestViewModel>();
                cfg.CreateMap<HotelAddRequestViewModel, Hotel>();
                cfg.CreateMap<Hotel, HotelUpdateRequestViewModel>();
                cfg.CreateMap<HotelUpdateRequestViewModel, Hotel>();
            });

            _mapper = config.CreateMapper();
        }

        public HotelViewModel MapToViewModel(Hotel hotel)
        {
            return _mapper.Map<HotelViewModel>(hotel);
        }

        public Hotel MapToAddEntity(HotelAddRequestViewModel hotelAddRequestViewModel)
        {
            return _mapper.Map<Hotel>(hotelAddRequestViewModel);
        }

        public Hotel MapToUpdateEntity(HotelUpdateRequestViewModel hotelUpdateRequestViewModel)
        {
            return _mapper.Map<Hotel>(hotelUpdateRequestViewModel);
        }

        public IEnumerable<HotelViewModel> MapToViewModelList(IEnumerable<Hotel> hotels)
        {
            return _mapper.Map<IEnumerable<HotelViewModel>>(hotels);
        }
    }
}
