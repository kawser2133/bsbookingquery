﻿using AutoMapper;
using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Interfaces.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSBookingQuery.Core.Mapper
{
    public class CommentMapper : ICommentMapper
    {
        private readonly IMapper _mapper;

        public CommentMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Comment, CommentViewModel>();
                cfg.CreateMap<CommentViewModel, Comment>();
                cfg.CreateMap<Comment, CommentAddRequestViewModel>();
                cfg.CreateMap<CommentAddRequestViewModel, Comment>();
                cfg.CreateMap<Comment, ReplyAddRequestViewModel>();
                cfg.CreateMap<ReplyAddRequestViewModel, Comment>();
                cfg.CreateMap<Comment, CommentUpdateRequestViewModel>();
                cfg.CreateMap<CommentUpdateRequestViewModel, Comment>();
            });

            _mapper = config.CreateMapper();
        }

        public CommentViewModel MapToViewModel(Comment comment)
        {
            return _mapper.Map<CommentViewModel>(comment);
        }

        public Comment MapToAddEntity(CommentAddRequestViewModel commentAddRequestViewModel)
        {
            return _mapper.Map<Comment>(commentAddRequestViewModel);
        }

        public Comment MapToReplyEntity(ReplyAddRequestViewModel replyAddRequestViewModel)
        {
            return _mapper.Map<Comment>(replyAddRequestViewModel);
        }

        public Comment MapToUpdateEntity(CommentUpdateRequestViewModel commentUpdateRequestViewModel)
        {
            return _mapper.Map<Comment>(commentUpdateRequestViewModel);
        }

        public IEnumerable<CommentViewModel> MapToViewModelList(IEnumerable<Comment> comments)
        {
            return _mapper.Map<IEnumerable<CommentViewModel>>(comments);
        }
    }
}
