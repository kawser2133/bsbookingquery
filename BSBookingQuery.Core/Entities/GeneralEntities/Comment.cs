﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BSBookingQuery.Core.Entities.GeneralEntities
{
    public class Comment : BaseEntity<int>
    {
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string CommentText { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Author { get; set; }
        [Required]
        public int HotelId { get; set; }
        [ForeignKey("HotelId")]
        public virtual Hotel Hotel { get; set; }
        public int? ParentCommentId { get; set; }
        [ForeignKey("ParentCommentId")]
        public virtual Comment ParentComment { get; set; }
        public ICollection<Comment> Replies { get; set; }
    }
}
