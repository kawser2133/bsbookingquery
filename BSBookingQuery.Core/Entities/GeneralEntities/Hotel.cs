﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BSBookingQuery.Core.Entities.GeneralEntities
{
    public class Hotel : BaseEntity<int>
    {
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Name { get; set; }
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Location { get; set; }
        public int Rating { get; set; }
        [StringLength(500, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Description { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
