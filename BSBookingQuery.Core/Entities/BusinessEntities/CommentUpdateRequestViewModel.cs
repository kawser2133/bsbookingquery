﻿using System.ComponentModel.DataAnnotations;

namespace BSBookingQuery.Core.Entities.BusinessEntities
{
    public class CommentUpdateRequestViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string CommentText { get; set; }
    }
}
