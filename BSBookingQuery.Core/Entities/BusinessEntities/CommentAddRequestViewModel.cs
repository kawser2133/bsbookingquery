﻿using System.ComponentModel.DataAnnotations;

namespace BSBookingQuery.Core.Entities.BusinessEntities
{
    public class CommentAddRequestViewModel
    {
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string CommentText { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Author { get; set; }
    }
}
