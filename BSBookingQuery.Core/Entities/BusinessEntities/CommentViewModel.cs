﻿
using System;
using System.Collections.Generic;

namespace BSBookingQuery.Core.Entities.BusinessEntities
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string CommentText { get; set; }
        public string Author { get; set; }
        public int HotelId { get; set; }
        public int? ParentCommentId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public List<CommentViewModel> Replies { get; set; }
    }
}
