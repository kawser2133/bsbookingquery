﻿using System.ComponentModel.DataAnnotations;

namespace BSBookingQuery.Core.Entities.BusinessEntities
{
    public class HotelAddRequestViewModel
    {
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Name { get; set; }
        [Required]
        [StringLength(200, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Location { get; set; }
        public int Rating { get; set; }
        [StringLength(500, ErrorMessage = "{0} can have a max of {1} characters")]
        public string Description { get; set; }
    }
}
