﻿using BSBookingQuery.Core.Entities.GeneralEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Core.Interfaces.Repositories
{
    public interface IHotelRepository : IBaseRepository<Hotel>
    {
        Task<IEnumerable<Hotel>> GetHotelsByName(string name);
        Task<IEnumerable<Hotel>> GetHotelsByLocation(string location);
        Task<IEnumerable<Hotel>> GetHotelsByRatingRange(int minRating, int maxRating);
    }
}
