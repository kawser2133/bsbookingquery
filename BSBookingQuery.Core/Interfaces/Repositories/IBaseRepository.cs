﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Core.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        Task<T> AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
        Task<T> GetByIdAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        Task SaveChangesAsync();
    }
}
