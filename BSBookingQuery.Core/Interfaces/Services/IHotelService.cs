﻿using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Core.Interfaces.Services
{
    public interface IHotelService
    {
        Task<HotelViewModel> GetHotelById(int id);

        Task<IEnumerable<HotelViewModel>> GetAllHotels();

        Task<IEnumerable<HotelViewModel>> GetHotelsByName(string name);

        Task<IEnumerable<HotelViewModel>> GetHotelsByLocation(string location);

        Task<IEnumerable<HotelViewModel>> GetHotelsByRatingRange(int minRating, int maxRating);

        Task<HotelViewModel> CreateHotel(HotelAddRequestViewModel addRequestViewModel);

        Task UpdateHotel(HotelUpdateRequestViewModel updateRequestViewModel);

        Task DeleteHotel(int id);
    }
}
