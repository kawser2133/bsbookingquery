﻿using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Core.Interfaces.Services
{
    public interface ICommentService
    {
        Task<CommentViewModel> GetCommentById(int id);
        Task<IEnumerable<CommentViewModel>> GetAllComments();
        Task<IEnumerable<CommentViewModel>> GetCommentsByHotelId(int hotelId);
        Task<CommentViewModel> AddComment(int hotelId, CommentAddRequestViewModel addRequestViewModel);
        Task<CommentViewModel> AddReply(int commentId, ReplyAddRequestViewModel addRequestViewModel);
        Task UpdateComment(CommentUpdateRequestViewModel updateRequestViewModel);
        Task DeleteComment(int id);
    }
}
