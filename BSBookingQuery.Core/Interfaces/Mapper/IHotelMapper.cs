﻿using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSBookingQuery.Core.Interfaces.Mapper
{
    public interface IHotelMapper
    {
        HotelViewModel MapToViewModel(Hotel hotel);
        Hotel MapToAddEntity(HotelAddRequestViewModel hotelAddRequestViewModel);
        Hotel MapToUpdateEntity(HotelUpdateRequestViewModel hotelUpdateRequestViewModel);
        IEnumerable<HotelViewModel> MapToViewModelList(IEnumerable<Hotel> hotels);
    }
}
