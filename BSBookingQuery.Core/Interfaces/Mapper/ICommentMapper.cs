﻿using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSBookingQuery.Core.Interfaces.Mapper
{
    public interface ICommentMapper
    {
        CommentViewModel MapToViewModel(Comment comment);
        Comment MapToAddEntity(CommentAddRequestViewModel commentAddRequestViewModel);
        Comment MapToReplyEntity(ReplyAddRequestViewModel replyAddRequestViewModel);
        Comment MapToUpdateEntity(CommentUpdateRequestViewModel commentUpdateRequestViewModel);
        IEnumerable<CommentViewModel> MapToViewModelList(IEnumerable<Comment> comments);
    }
}
