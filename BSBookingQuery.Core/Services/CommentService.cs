﻿using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Exceptions;
using BSBookingQuery.Core.Interfaces.Mapper;
using BSBookingQuery.Core.Interfaces.Repositories;
using BSBookingQuery.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Core.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IHotelRepository _hotelRepository;
        private readonly ICommentMapper _mapper;

        public CommentService(ICommentRepository commentRepository, ICommentMapper mapper, IHotelRepository hotelRepository)
        {
            _commentRepository = commentRepository;
            _mapper = mapper;
            _hotelRepository = hotelRepository;
        }

        public async Task<CommentViewModel> GetCommentById(int id)
        {
            var comment = await _commentRepository.GetByIdAsync(id);
            if (comment == null)
            {
                throw new NotFoundException($"Comment with ID- {id} not found.");
            }

            return _mapper.MapToViewModel(comment);
        }

        public async Task<IEnumerable<CommentViewModel>> GetAllComments()
        {
            var comments = await _commentRepository.GetAllAsync();
            return _mapper.MapToViewModelList(comments.Where(x => x.ParentCommentId == null));
        }

        public async Task<IEnumerable<CommentViewModel>> GetCommentsByHotelId(int hotelId)
        {
            var comments = await _commentRepository.GetCommentsByHotelId(hotelId);
            return _mapper.MapToViewModelList(comments.Where(x => x.ParentCommentId == null));
        }

        public async Task<CommentViewModel> AddComment(int hotelId, CommentAddRequestViewModel addRequestViewModel)
        {
            var hotelExistingData = await _hotelRepository.GetByIdAsync(hotelId);
            if (hotelExistingData == null)
            {
                throw new NotFoundException($"Hotel with ID- {hotelId} not found.");
            }

            var mapAddData = _mapper.MapToAddEntity(addRequestViewModel);

            // update existing data
            mapAddData.CreatedAt = DateTime.Now;
            mapAddData.HotelId = hotelId;
            var addData = await _commentRepository.AddAsync(mapAddData);
            return _mapper.MapToViewModel(addData);
        }

        public async Task<CommentViewModel> AddReply(int commentId, ReplyAddRequestViewModel addRequestViewModel)
        {
            var parentCommentData = await _commentRepository.GetByIdAsync(commentId);

            if (parentCommentData == null)
            {
                throw new NotFoundException($"Comment with ID- {commentId} not found.");
            }

            var hotelData = await _hotelRepository.GetByIdAsync(parentCommentData.HotelId);

            if (hotelData == null)
            {
                throw new NotFoundException($"Hotel with ID- {parentCommentData.HotelId} not found.");
            }

            var mapAddData = _mapper.MapToReplyEntity(addRequestViewModel);

            // update existing data
            mapAddData.CreatedAt = DateTime.Now;
            mapAddData.HotelId = hotelData.Id;
            mapAddData.ParentCommentId = parentCommentData.Id;

            var addData = await _commentRepository.AddAsync(mapAddData);
            return _mapper.MapToViewModel(addData);
        }

        public async Task UpdateComment(CommentUpdateRequestViewModel updateRequestViewModel)
        {
            var commentExistingData = await _commentRepository.GetByIdAsync(updateRequestViewModel.Id);
            if (commentExistingData == null)
            {
                throw new NotFoundException($"Comment with ID- {updateRequestViewModel.Id} not found.");
            }

            // update existing data
            commentExistingData.UpdatedAt = DateTime.Now;
            commentExistingData.CommentText = updateRequestViewModel.CommentText;

            _commentRepository.Update(commentExistingData);
            await _commentRepository.SaveChangesAsync();
        }

        public async Task DeleteComment(int id)
        {
            var commentExistingData = await _commentRepository.GetByIdAsync(id);
            if (commentExistingData == null)
            {
                throw new NotFoundException($"Comment with ID- {id} not found.");
            }
            _commentRepository.Delete(commentExistingData);
            await _commentRepository.SaveChangesAsync();
        }

    }
}
