﻿using AutoMapper;
using BSBookingQuery.Core.Entities.BusinessEntities;
using BSBookingQuery.Core.Entities.GeneralEntities;
using BSBookingQuery.Core.Exceptions;
using BSBookingQuery.Core.Interfaces.Mapper;
using BSBookingQuery.Core.Interfaces.Repositories;
using BSBookingQuery.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSBookingQuery.Core.Services
{
    public class HotelService : IHotelService
    {
        private readonly IHotelRepository _hotelRepository;
        private readonly IHotelMapper _mapper;

        public HotelService(IHotelRepository hotelRepository, IHotelMapper mapper)
        {
            _hotelRepository = hotelRepository;
            _mapper = mapper;
        }

        public async Task<HotelViewModel> GetHotelById(int id)
        {
            var hotel = await _hotelRepository.GetByIdAsync(id);
            if (hotel == null)
            {
                throw new NotFoundException($"Hotel with ID- {id} not found.");
            }

            return _mapper.MapToViewModel(hotel);
        }

        public async Task<IEnumerable<HotelViewModel>> GetAllHotels()
        {
            var hotels = await _hotelRepository.GetAllAsync();
            return _mapper.MapToViewModelList(hotels);
        }

        public async Task<IEnumerable<HotelViewModel>> GetHotelsByName(string name)
        {
            var hotels = await _hotelRepository.GetHotelsByName(name);
            return _mapper.MapToViewModelList(hotels);
        }

        public async Task<IEnumerable<HotelViewModel>> GetHotelsByLocation(string location)
        {
            var hotels = await _hotelRepository.GetHotelsByLocation(location);
            return _mapper.MapToViewModelList(hotels);
        }

        public async Task<IEnumerable<HotelViewModel>> GetHotelsByRatingRange(int minRating, int maxRating)
        {
            var hotels = await _hotelRepository.GetHotelsByRatingRange(minRating, maxRating);
            return _mapper.MapToViewModelList(hotels);
        }

        public async Task<HotelViewModel> CreateHotel(HotelAddRequestViewModel addRequestViewModel)
        {
            var mapAddData = _mapper.MapToAddEntity(addRequestViewModel);

            // update existing data
            mapAddData.CreatedAt = DateTime.Now;

            var addData = await _hotelRepository.AddAsync(mapAddData);
            return _mapper.MapToViewModel(addData);
        }

        public async Task UpdateHotel(HotelUpdateRequestViewModel updateRequestViewModel)
        {
            var hotelExistingData = await _hotelRepository.GetByIdAsync(updateRequestViewModel.Id);
            if (hotelExistingData == null)
            {
                throw new NotFoundException($"Hotel with ID- {updateRequestViewModel.Id} not found.");
            }

            // update existing data
            hotelExistingData.UpdatedAt = DateTime.Now;
            hotelExistingData.Name = updateRequestViewModel.Name;
            hotelExistingData.Location = updateRequestViewModel.Location;
            hotelExistingData.Rating = updateRequestViewModel.Rating;
            hotelExistingData.Description = updateRequestViewModel.Description;

            _hotelRepository.Update(hotelExistingData);
            await _hotelRepository.SaveChangesAsync();
        }

        public async Task DeleteHotel(int id)
        {
            var hotelExistingData = await _hotelRepository.GetByIdAsync(id);
            if (hotelExistingData == null)
            {
                throw new NotFoundException($"Hotel with ID- {id} not found.");
            }
            _hotelRepository.Delete(hotelExistingData);
            await _hotelRepository.SaveChangesAsync();
        }
    }
}
